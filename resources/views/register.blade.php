<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Register</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="POST">
        @csrf
        <h3>Sign Up Form</h3>
        <label for="fn"> First name:</label><br><br>
        <input type="text" id="fn" placeholder="Input here..." name="fn"><br><br>

        <label for="ln">Last name:</label><br><br>
        <input type="text" id="ln" placeholder="Input here..." name="ln"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="0" checked>Male <br>
        <input type="radio" name="gender" value="1">Female <br>
        <input type="radio" name="gender" value="2">Other <br><br>

        <label> Nationality:</label><br><br>
        <select name="nationality">
            <optgroup label="Asia">
                <option value="id" selected>Indonesia</option>
                <option value="ml">Malaysia</option>
                <option value="jp">Jepang</option>
            </optgroup>
            <optgroup label="Eropa">
                <option value="bl">Belanda</option>
                <option value="gr">German</option>
                <option value="pr">Prancis</option>
            </optgroup>
        </select><br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="Language" value="0" checked>Bahasa Indonesia <br>
        <input type="checkbox" name="Language" value="1">English <br>
        <input type="checkbox" name="Language" value="2">Other <br><br>

        <label>Bio:</label><br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
    <p>&copy;Diki Somantri</p>
</body>

</html>