<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }
    public function welcome()
    {
        return view('welcome');
    }
    public function welcome_post(Request $request)
    {
        $fn = $request["fn"];
        $ln = $request["ln"];
        return view('welcome', ["fn" => $fn, "ln" => $ln]);
    }
}
